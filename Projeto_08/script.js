//aula 17
/* var -> vaza do escopo, valor pode ser alterado
   let -> apenas pode ser usado dentro do escopo em que fora criado, exemplo: se o mesmo for declarado em um If/Else, apenas dentro do mesmo poderá ser utilizado.
   const  -> constante, uma variavel imutavel, nao muda o valor
   para nao vazar da funcao, o melhor e se utilizar LET

   spread operators -> Ele é representado por três pontos, sim, o Spread operator é o famoso ‘…’ (três pontinhos para os mais íntimos). 
                       Ele basicamente permite que expressões expandam o conteúdo de arrays em locais onde múltiplos elementos são esperados.
*/

function cadastroPessoa(info){
   let novosDados  = {
      ...info,
      cargo: 'Programador',
      status: 1,
      codigo: '0112198833'
   }
   return novosDados;
}

console.log(cadastroPessoa({nome: 'Tiago', sobrenome: 'Andrade', anoInicio:  2045}));