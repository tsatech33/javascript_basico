//Web Storage  -> localStorage e sessionStorage 
//Cookie -> grava informações no navegador do usuario
//Session -> Ao fechar o site/navegador, os dados se apagam
//localStorage.setItem("nome", "XXXXXX");
//localStorage.getItem("nome");
//localStorage.removeItem("nome");

//var nome = localStorage.nome;
//console.log(nome);

var nome = '';

if (typeof localStorage.nome == 'undefined') {
    localStorage.nome = prompt("Digite seu nome?");
}

nome  = localStorage.nome;

document.getElementById('nome').innerHTML = nome;