/**Funções anônimas */

function adicionar(...numeros){
    /*forma 1 de criar a função */
    /*let total =  numeros.reduce((total, proximo) => {
        return total + proximo;
    });*/

    /**  forma simplificada */
    let total =  numeros.reduce((total, proximo) => total + proximo);
    console.log(total);
}

adicionar(1,2,3,4,5);