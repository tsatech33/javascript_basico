/**Metodos
 * includes -> O método includes() determina se um array contém um determinado elemento, retornando true ou false apropriadamente
 * 
 * endsWith
 * 
 * startsWith
 * 
 * ambas sao case sensitive
 
 */

/*let nomes =  ['Tiago','Igor','Samuel'];

if (nomes.includes('Tiago')){
   console.log('Tiago está na lista');
}else{
   console.log('Tiago não está na lista');
};*/

let nome = 'Tiago';

console.log(nome.endsWith('s'));
console.log(nome.startsWith('T'));