//aula 18
/* 

   rest operator -> Se o último argumento nomeado de uma função tiver prefixo com  ..., ele irá se tornar um array em que os elemento de 0 (inclusive) até
                    theArgs.length (exclusivo) são disponibilizados pelos argumentos atuais passados à função.No exemplo acima, theArgs irá coletar o terceiro argumento 
                    da função (porquê o primeiro é mapeado para a, e o segundo para b) e assim por diante em todos os argumentos consecutivos.

function cadastroPessoa(info){
   let novosDados  = {
      ...info,
      cargo: 'Programador',
      status: 1,
      codigo: '0112198833'
   }
   return novosDados;
}

console.log(cadastroPessoa({nome: 'Tiago', sobrenome: 'Andrade', anoInicio:  2045}));
*/

/*function minhaLista(...nomes){
   console.log(nomes);
}

minhaLista("Tiago","Igor","Samuel");

function minhaListaNumero(...numeros){
   console.log(numeros);
}

minhaListaNumero(1,2,3,4,5);*/


function cadastrarUsuario(usuarios, ...novosUsuarios){
   let totalUsuarios  =[
      ...usuarios,
      ...novosUsuarios
   ];
   return console.log(totalUsuarios);

}

let  usuarios = ["Tiago","Igor","Samuel"];
let novosUsuarios=  cadastrarUsuario(usuarios, "J.Gaviao", "Drika");
